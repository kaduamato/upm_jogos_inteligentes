# Questões GA

* Como representar um caminho a ser percorrido por um NPC?

    O caminho a ser percorrido pode ser representado por meio de um vetor de genes cotendo dois alelos. Esse fenótipo pode ser definido com o primeiro alelo sendo a direção a ser percorrida e o segundo alelo sendo a quantidade de passos a ser dados nessas direção.

* Como definir a população inicial (caminhos/cromossomos)?

     A população inicial é definida de forma aleátória.
     
* Como avaliar a aptidão de cada cromossomo?

     Para avaliar a aptidão de um cromosso necessário descobrir qual cromossomo chega mais perto do objetivo em menos "tempo". Quanto melhor essa relação mais aptidão tem.

* Como selecionar os pais para gerar novos seres (filhos)?

    É necessaário que o código escolha os dois indivíduos com a maior aptidão e assim cruze eles.

* Como fazer o cruzamento dos pais?
 
    Pegue uma parte dos genes de um dos pais e outra aparte dos genes do outro pai. Deixando os cromossomos filhos com a mesma quantidade de genes.

* Como um filho pode sofrer mutação?
 
    Para ser feita uma mutação é necessário que se altere um gene do cromossomo filho. Há uma porcentagem de chance dessa mutação acontecer.

* Como criar uma nova população/geração?
 
     Você cria uma nova geração após cruzar os pais de várias formas, ou seja, combinando diferentes genes de um e de outro. 
     Essa geração terá o mesmo número de indivíduos que a inicial.
