﻿using System;
using System.IO;
using System.Globalization;
using Microsoft.ML;
using Microsoft.ML.Data;

namespace IrisFlowerClustering
{
    static class Extension
    {
        public static string UnitToString(this uint value)
        {
            switch (value)
            {
                case 1:
                    return "Iris-Setosa";
                case 2:
                    return "Iris-Versicolor";
                case 3:
                    return "Iris-Virginica";
            }

            return "";
        }

        public static IrisData FloatVectorToIrisData(this float[] value)
        {
            return new IrisData
            {
                SepalLength = value[0],
                SepalWidth = value[1],
                PetalLength = value[2],
                PetalWidth = value[3]
            };
        } 
    }
    class Program
    {
        static readonly string _trainPath = Path.Combine(Environment.CurrentDirectory, "Data", "iris.train.txt");
        static readonly string _testPath = Path.Combine(Environment.CurrentDirectory, "Data", "iris.test.txt");
        static readonly string _modelPath = Path.Combine(Environment.CurrentDirectory, "Data", "IrisClusteringModel.zip");

        static void Main(string[] args)
        {
            NumberFormatInfo number = (NumberFormatInfo)CultureInfo.CurrentCulture.NumberFormat.Clone();
            number.NumberDecimalSeparator = ".";

            var mlContext = new MLContext(seed: 0);

            IDataView dataView = mlContext.Data.LoadFromTextFile<IrisData>(_trainPath, hasHeader: false, separatorChar: ',');

            string featuresColumnName = "Features";
            var pipeline = mlContext.Transforms
                .Concatenate(featuresColumnName, "SepalLength", "SepalWidth", "PetalLength", "PetalWidth")
                .Append(mlContext.Clustering.Trainers.KMeans(featuresColumnName, numberOfClusters: 3));

            var model = pipeline.Fit(dataView);

            using (var fileStream = new FileStream(_modelPath, FileMode.Create, FileAccess.Write, FileShare.Write))
            {
                mlContext.Model.Save(model, dataView.Schema, fileStream);
            }

            var predictor = mlContext.Model.CreatePredictionEngine<IrisData, ClusterPrediction>(model);

            string[] test = File.ReadAllLines(_testPath);

            for (int i = 0; i < test.Length; i++)
            {
                string[] temp = test[i].Split(",");

                float[] tempFloat = new float[temp.Length - 1];

                for (int j = 0; j < temp.Length - 1; j++)
                {
                    tempFloat[j] = float.Parse(temp[j], number);
                }
                
                var prediction = predictor.Predict(tempFloat.FloatVectorToIrisData());

                Console.WriteLine($"Cluster: {prediction.PredictedClusterId.UnitToString()}");
            }
        }
    }
}
