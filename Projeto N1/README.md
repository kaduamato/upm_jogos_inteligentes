Funcionamento Algorítimo Genético para resolução do probleam do quadrado mágico

O algorítimo inicia quando o a pessoa escreve o tamanho do lado do quadrado e seleciona start.
O Código então começa Criando a primeira população de individuos. O método construtor do indivíduo então cria o cromossomo do individuo aleatorizando as psoições
dos números de 1 a N (sendo n a área do cubo);
Após isso, o algoritimo começa a calcular o fitness de cada individuo da população.
A apitidão é calculada da seguinte forma, priemiro o código vê qual deveria ser a soma das linhas, colunas e diagonais a partir da fórmula L+(L^3)/2 onde L é o tamanho do lado do quadrado.
Depois disso é feito o cálculo da soma de cada linha, coluna e diagonal. O módulo da subitração entre o resultado da fórmula menos o resultado da soma dos valores de uma linha vai ser somado depois
como os resultados do mesmo cáculo de cada linha, coluna e diagonal.
Após todas os Fitness calculados o código separa o individuo com o melhor Fitness para o cruzamento, ou seja, o fitness mais perto do 0. Caso algum individuo tenha o fitness zero, o código para o processamento
e exibe o cromosso com fitness zero na tela. Caso não tenha nenhum individuo com fitiness 0 o código chama o cruzamento para criar a nova população.
O primeiro parente é aquele com maior fitness da geração anterior. Já o segundo parente é o proxímo individuo da lista após o individuo com maior fitness.
Como os vetores dos parentes não podem simplesmente serem embaralhados por não poder alterar os valores do vector (1 a n) o crossover é feito da seguinte forma:
É criado uma vetor inverso para cada um dos parentes. Esse vector inverso vai ter o mesmo tamanho do vetor pai e vai conter os valores da seguinte forma
Temos como exemplo o vetor pai [2,4,3,1]. Para criar o vetor inverso localizamos onde está o valor 1. No caso está na 4 casa do vetor. Após isso contamos quantos valores a esquerda da quarta casa são maior que 1, no caso 3 valores. Portanto o valor indice i = 0 do vetor inverso será 3 [3,_,_,_].
Repitimos esse mesmo processo até completar o vetor inverso dos dois parentes. No caso do exemplo o vetor final seria [3, 0, 1 ,0].
Com o vetor dessa forma é possível fazer um crossover simples onde um númeor aletório vai escolher se o alelo usado será do parente um e parente dois.
Após ser realizada essa troca é preciso converter novamente os vetores inversos para os normais.
A primeira parte da transição é feita da seguinte forma, pegasse o vetor inverso [3, 0, 1, 0] e monta um novo vetor de posições de traz para frente.
Cada número colocado no vetor vai checar se há um numero maior ou igual em seus index a direita se tiver vai incrementar mais um nesses números.
Ordem do processo
inv = [3, 0, 1, 0]
pos = [ , , , 0] - Nenhum número a direita

inv = [3, 0, 1, 0]
pos = [ , , 1, 0] - Nenhum número maior ou igual a direita
 
inv = [3, 0, 1, 0]
pos = [ , 0, 1, 0] - 1 é maior e 0 é igual e estão a direita portanto vetor fica assim [, 0, 2, 1]

inv = [3, 0, 1, 0]
pos = [3, 0, 2, 1] - Nenhum número maior ou igual a direita

Após esse processo temos o um vetor de posição no qual os valores são index do vetor final do filho e a posição em que eles estão são os valores no vetor final
[3, 0, 2, 1]
[2, 4, 3, 1]

A mutação é feita do logo depois desse processo trocando valores de posição dentor do vetor filho.
Após o individuo é adicionado a população. Quando a nova população está pronta o processo todo se repete ate se achar um resultado final.

Para se fazer um teste digite um valor para o lado do quadrado no inputfield da unity e clique no botão de start. Para teste é melhor utilizar quadrados com lados pequenos como 3 e 4. 


