﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace Backpropagation
{
    static class Extensions
    {
        public static double Convert(this String str)
        {
            switch (str)
            {
                case "Iris-setosa":
                    return 0.0;
                case "Iris-versicolor":
                    return 0.5;
                case "Iris-virginica":
                    return 1.0;
            }

            return -1.0;
        }

        public static string Decode(this double dob)
        {
            if (dob < 0.33)
            {
                return "Iris-setosa";
            }
            else if (dob > 0.67)
            {
                return "Iris-virginica";
            }
            else
            {
                return "Iris-versicolor";
            }
        }

        public static double Derivative(this double dob)
        {
            return dob * (1 - dob);
        }

        public static double Error(this double dob, double expected)
        {
            return expected - dob;
        }

        public static double MeanSquaredError(this double[] deltaError)
        {
            double MSE = 0.0;
            for (int i = 0; i < deltaError.Length; i++)
            {
                MSE += deltaError[i] * deltaError[i];
            }

            return MSE;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            const int numberOfInputs = 4;
            const int numberOfHidden = 5;
            const int numberOfOutputs = 3;
            const int maxGenerations = 30000;

            const double learningRate = 0.01;
            const double MSE_Break = 0.05;
            const double multiplier = 0.9;

            double[][] wHidden = new double[numberOfHidden][];
            double[][] wOut = new double[numberOfOutputs][];
            double[][] trainInputs;
            double[][] testInputs;

            string trainLink = Directory.GetCurrentDirectory() + "\\..\\..\\..\\..\\iris.train.txt";
            string testLink = Directory.GetCurrentDirectory() + "\\..\\..\\..\\..\\iris.test.txt";

            List<double> convertedExpectedResults = new List<double>();

            Random random = new Random((int)DateTime.UtcNow.Ticks);

            SetUpToStart();
            trainInputs = ReadFile(trainLink);

            int generation = 0;
            TrainAndPrintWeigths();

            testInputs = ReadFile(testLink);

            for (int i = 0; i < testInputs.GetLength(0); i++)
            {
                Console.WriteLine(Validate(testInputs[i]));
            }

            void RandomizeWeights(double[][] matrix, int numberOfColumns)
            {
                for (int i = 0; i < matrix.GetLength(0); i++)
                {
                    matrix[i] = new double[numberOfColumns];

                    for (int j = 0; j < numberOfColumns; j++)
                    {
                        matrix[i][j] = random.NextDouble();
                    }
                }
            }

            void PrintWeights(double[][] matrix, bool trained, string layerName, int numberOfColumns)
            {
                if (trained)
                {
                    Console.WriteLine($"Pesos depois do treino na camada {layerName}: ");
                }
                else
                {
                    Console.WriteLine($"Pesos antes do treino na camada {layerName}: ");
                }

                for (int i = 0; i < matrix.GetLength(0); i++)
                {
                    for (int j = 0; j < numberOfColumns; j++)
                    {
                        Console.WriteLine(matrix[i][j]);
                    }
                }
            }

            void TrainAndPrintWeigths()
            {
                Training();
                PrintWeights(wHidden, true, "Oculta", numberOfInputs);
                PrintWeights(wOut, true, "Saída", numberOfHidden);
            }

            void SetUpToStart()
            {
                RandomizeWeights(wHidden, numberOfInputs);
                RandomizeWeights(wOut, numberOfHidden);
                PrintWeights(wHidden, false, "Oculta", numberOfInputs);
                PrintWeights(wOut, false, "Saída", numberOfHidden);
            }

            double[][] ReadFile(string file)
            {
                NumberFormatInfo number = (NumberFormatInfo)CultureInfo.CurrentCulture.NumberFormat.Clone();
                number.NumberDecimalSeparator = ".";

                string[] fileLines = File.ReadAllLines(file);
                double[][] inputsReturn = new double[fileLines.Length][];
                convertedExpectedResults.Clear();

                for (int i = 0; i < fileLines.Length; i++)
                {
                    string[] line = fileLines[i].Split(",");

                    inputsReturn[i] = new double[numberOfInputs];

                    for (int j = 0; j < line.Length - 1; j++)
                    {
                        inputsReturn[i][j] = double.Parse(line[j], number);
                    }

                    convertedExpectedResults.Add(line[^1].Convert());
                }

                return inputsReturn;
            }

            double[] FeedForward(int numberOfNerouns, double[] inputs, double[][] weigths)
            {
                double[] outputs = new double[numberOfNerouns];

                for (int i = 0; i < outputs.Length; i++)
                {
                    outputs[i] = Perceptron.CalculateOutput(inputs, weigths[i]);
                }

                return outputs;
            }

            void Training()
            {
                Func<double, double, double, double> UpdateWeights = (LR, input, error) => LR * input * error;

                while (true)
                {
                    int count = 0;

                    for (int i = 0; i < trainInputs.GetLength(0); i++)
                    {
                        double[] hiddenOutputs = FeedForward(numberOfHidden, trainInputs[i], wHidden);
                        double[] outOutputs = FeedForward(numberOfOutputs, hiddenOutputs, wOut);

                        double[] outputErrors = new double[numberOfOutputs];
                        double[] deltaErrors = new double[numberOfOutputs];
                        double sumOfErrors = 0;

                        for (int j = 0; j < numberOfOutputs; j++)
                            deltaErrors[j] = outOutputs[j].Error(convertedExpectedResults[i]);

                        if (deltaErrors.MeanSquaredError() < MSE_Break)
                            count++;

                        for (int j = 0; j < numberOfOutputs; j++)
                        {
                            outputErrors[j] = deltaErrors[j] * outOutputs[j].Derivative();
                            sumOfErrors += outputErrors[j];

                            for (int h = 0; h < wOut[j].Length; h++)
                            {
                                wOut[j][h] += UpdateWeights(learningRate, hiddenOutputs[h], outputErrors[j]);
                            }
                        }

                        double hiddenError = 0.0;

                        for (int j = 0; j < numberOfHidden; j++)
                        {
                            hiddenError = hiddenOutputs[j].Derivative() * sumOfErrors;

                            for (int h = 0; h < wHidden[j].Length; h++)
                            {
                                wHidden[j][h] += UpdateWeights(learningRate, trainInputs[i][h], hiddenError);
                            }
                        }
                    }

                    generation++;
                    if (count >= trainInputs.GetLength(0) * multiplier)
                    {
                        break;
                    }
                    else if (generation >= maxGenerations)
                    {
                        generation = 0;
                        SetUpToStart();
                        TrainAndPrintWeigths();
                        break;
                    }
                }
            }

            string Validate(double[] line)
            {
                double[] hiddenOutputs = new double[numberOfHidden];
                double[] outOutputs = new double[numberOfOutputs];
               
                hiddenOutputs = FeedForward(numberOfHidden, line, wHidden);
                outOutputs = FeedForward(numberOfOutputs, hiddenOutputs, wOut);

                Console.WriteLine(outOutputs[0]);

                return outOutputs[0].Decode();
            }

            Console.ReadKey();
        }
    }

    class Perceptron
    {
        public static double CalculateOutput(double[] inputs, double[] weights)
        {
            double sumReult = 0.0;

            for (int i = 0; i < inputs.Length; i++)
            {
                sumReult += inputs[i] * weights[i];
            }

            return 1 / (1 + Math.Exp(-sumReult));

        }
    }
}
