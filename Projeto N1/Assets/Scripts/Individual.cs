﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Individual
{
    [SerializeField] int tempSize;

    int fitness;
    public int Fitness
    {
        get { return fitness; }
        set { fitness = value; }
    }
    public List<int> genomes;
           
    public Individual(int size)
    {
        int area = size*size;

        genomes = new List<int>(area);

        for (int i = 0; i < area; i++)
        {
            bool canUse = false;
            int tempValue = 0;
            while (!canUse)
            {
                tempValue = Random.Range(1, area+1);
                canUse = CheckValue(tempValue);
            }
            genomes.Add(tempValue);
        }
        fitness = 100;
    }

    public Individual(List<int> chromosome)
    {
        genomes = chromosome;
        fitness = 100;
    }


    bool CheckValue(int value)
    {
        for (int i=0; i < genomes.Count; i++)
        {
            if (genomes[i] == value)
            {
                return false;
            }
        }

        return true;
    }
}
