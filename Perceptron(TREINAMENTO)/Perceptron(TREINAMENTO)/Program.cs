﻿using System;
using System.Collections.Generic;

namespace Perceptron_TREINAMENTO_
{
    class Program
    {
        static void Main(string[] args)
        {
            double[,] inputs = { { 0.3, 0.7 }, { -0.6, 0.3 }, { -0.1, -0.8 }, { 0.1, -0.45 } };
            double[] classes = { 1, 0, 0, 1 };
            double bias = 0;
            double[] weights = { 0.8, -0.5 };
            double learningRate = 0.5;

            for (int i = 0; i < inputs.GetLength(0); i++)
            {
                double output = CalculateOutput(inputs[i,0],inputs[i,1], weights, bias);
                double lost = CalculateLost(output);
                double error = CalculateError(classes[i], lost);
                UpdateWeights(weights, inputs[i, 0],inputs[i,1], learningRate, error);
                
            }
            Console.WriteLine($"Peso 1: {weights[0]}, Peso 2: {weights[1]}");
        }

        static double CalculateOutput(double input1, double input2, double[] weights, double bias)
        {
            double sumReult = 0.0f;
            double[] inputs = { input1, input2 };

            for (int i = 0; i < inputs.Length; i++)
            {
                sumReult += inputs[i] * weights[i];
            }

            sumReult = sumReult - bias;
 
            return sumReult;

        }

        static double CalculateLost(double output)
        {

            if (output >= 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        static double CalculateError(double classes, double lost)
        {
            return classes - lost;
        }

        static void UpdateWeights(double[] weights, double input1,double input2, double learningRate, double error)
        {
            double[] inputs = { input1, input2 };
            for (int i = 0; i < weights.Length; i++)
            {
                weights[i] = weights[i] + (learningRate * error * inputs[i]);
            }
        }
    }
}
