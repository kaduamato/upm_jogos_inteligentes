﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GA : MonoBehaviour
{
    List<Individual> population;
    [SerializeField] int squareSize;
    [SerializeField] int populationSize;

    [SerializeField] Text generationText;
    [SerializeField] Text answerText;
    [SerializeField] InputField inputField;
    [SerializeField] Button button;

    int expectedNumber;


    int bestFitness;
    int indivudualIndex;

    int generation;

    bool running;
    void Start()
    {
       
    }

    void Update()
    {
        if (running) Epoch();
    }


    public void Steup()
    {
        squareSize = int.Parse(inputField.text);
        inputField.interactable = false;
        button.interactable = false;
        answerText.text = "";
        StartGA();
    }

    void StartGA()
    {
        generation = 1;
        generationText.text = "Generation: " + generation;
        population = new List<Individual>();
        CreateFirstPopulation();
        expectedNumber = (squareSize + (squareSize * squareSize * squareSize)) / 2;
        running = true;
    }


    void CreateFirstPopulation()
    {
        for (int i = 0; i < populationSize; i++)
        {
            Individual child = new Individual(squareSize);
            population.Add(child);
        }

    }

    void Epoch()
    {
        if (!running) return;
        DecodeRoute();
        UpdateFitness();

        if (!running) return;
        {
            CreateNewPopulation();
            generation++;
            generationText.text = "Generation: " + generation;
        }
    }

    Individual CrossingParents(List<int> firstParent, List<int> secoundParent)
    {
        List<int> babyChromosome = new List<int>();

        List<int> firstInverse = Inversion(firstParent);
        List<int> secoundInverse = Inversion(secoundParent);

        List<int> inverseChromosome = new List<int>();
        for (int i = 0; i < firstParent.Count; i++)
        {
            int temp = Random.Range(0, 10);

            if (temp >= 0 && temp < 5)
            {
                inverseChromosome.Add(firstInverse[i]);
            }
            if (temp >= 5 && temp < 10)
            {
                inverseChromosome.Add(secoundInverse[i]);
            }
        }

        babyChromosome = Square(inverseChromosome);
        babyChromosome = Mutation(babyChromosome);

        return new Individual(babyChromosome);
    }

    List<int> Inversion(List<int> square)
    {
        int size = square.Count;
        int[] listToArray = square.ToArray();
        int[] inversion = new int[size];

        for (int i = 0, n = 1; i < size; i++, n++)
        {
            inversion[i] = 0;

            for (int j = 0; listToArray[j] != n; j++)
            {
                if (listToArray[j] > n) inversion[i]++;
            }
        }

        return ArrayToList(inversion);
    }

    List<int> Square(List<int> inv)
    {
        int size = inv.Count;
        int[] listToArray = inv.ToArray();
        int[] pos = new int[size];

        for (int i = size - 1; i >= 0; i--)
        {
            pos[i] = listToArray[i];

            for (int m = i + 1; m < size; m++)
            {
                if (pos[m] >= listToArray[i]) pos[m]++;
            }
        }

        int[] square = new int[size];
        for (int i = 0; i < size; i++) square[pos[i]] = i + 1;

        return ArrayToList(square);
    }



    void CreateNewPopulation()
    {
        List<Individual> childs = new List<Individual>();

        Individual firstParent = population[indivudualIndex];
        Individual secoundParent;
        if (indivudualIndex == populationSize - 1)
        {
            secoundParent = population[0];
        }
        else
        {
            secoundParent = population[indivudualIndex + 1];
        }

        for (int i = 0; i < populationSize; i++)
        {
            childs.Add(CrossingParents(firstParent.genomes, secoundParent.genomes));
        }

        population = childs;
    }

    List<int> Mutation(List<int> genomes)
    {
        List<int> returnList = genomes;

        int chance = Random.Range(0, 11);

        int firstAllele = 0;
        int secoundAllele = 0;

        if (chance >= 0 && chance < 3)
        {
            while (firstAllele == secoundAllele)
            {
                firstAllele = Random.Range(0, genomes.Count);
                secoundAllele = Random.Range(0, genomes.Count);
            }

            int temp = returnList[firstAllele];
            returnList[firstAllele] = returnList[secoundAllele];
            returnList[secoundAllele] = temp;
        }


        return returnList;
    }

    void DecodeRoute()
    {
        for (int i = 0; i < populationSize; i++)
        {
            population[i].Fitness = Lines(population[i].genomes) + Colums(population[i].genomes) + FirstDiagonal(population[i].genomes) + SecoundDiagonal(population[i].genomes);
        }
    }

    int Lines(List<int> genomes)
    {
        int count = 0;
        int result = 0;
        int temp = 0;

        for (int i = 0; i < squareSize * squareSize; i++)
        {
            temp = temp + genomes[i];
            count++;

            if (count == squareSize)
            {
                count = 0;
                result += Mathf.Abs(expectedNumber - temp);
                temp = 0;
            }
        }
        return result;
    }

    int Colums(List<int> genomes)
    {
        int result = 0;
        int temp = 0;

        for (int i = 0; i < squareSize; i++)
        {
            for (int j = i; j < squareSize * squareSize; j += squareSize)
            {
                temp += genomes[j];
            }
            result += Mathf.Abs(expectedNumber - temp);
            temp = 0;
        }
        return result;
    }

    int FirstDiagonal(List<int> genomes)
    {
        int result = 0;
        int temp = 0;

        for (int i = 0; i < squareSize * squareSize; i += squareSize + 1)
        {
            temp = temp + genomes[i];
        }

        result += Mathf.Abs(expectedNumber - temp);

        return result;
    }

    int SecoundDiagonal(List<int> genomes)
    {
        int result = 0;
        int temp = 0;

        for (int i = squareSize - 1; i <= squareSize * squareSize - squareSize; i += squareSize - 1)
        {
            temp = temp + genomes[i];
        }

        result += Mathf.Abs(expectedNumber - temp);

        return result;
    }


    void UpdateFitness()
    {
        indivudualIndex = 0;
        bestFitness = 100;

        for (int i = 0; i < populationSize; i++)
        {
            if (population[i].Fitness < bestFitness)
            {
                bestFitness = population[i].Fitness;
                indivudualIndex = i;

                if (population[i].Fitness == 0)
                {
                    running = false;
                    PrintInScreen(population[i].genomes);
                    inputField.interactable = true;
                    button.interactable = true;
                    return;
                }
            }
        }
    }

    List<int> ArrayToList(int[] array)
    {
        List<int> returnList = new List<int>();
        for (int i = 0; i < array.Length; i++)
        {
            returnList.Add(array[i]);
        }

        return returnList;
    }

    void PrintInScreen(List<int> genomes)
    {
        string finalString = "";
        int count = 0;
        for (int i = 0; i < genomes.Count; i++)
        {
            if (genomes[i] >= 1 && genomes[i] <= 9)
            {
                finalString += "|0" + genomes[i].ToString() + "| ";
            }
            else
            {
                finalString += "|" + genomes[i].ToString() + "| ";
            }
            count++;
            if (count == squareSize)
            {
                finalString += "$";
                count = 0;
            }

        }
        finalString = finalString.Replace('$', '\n');
        answerText.text = finalString;
    }
}
