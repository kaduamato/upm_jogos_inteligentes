﻿using System;

namespace Perceptron
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Digite quantos inputs terão no seu perceptron: ");
            int qtdInpts = int.Parse(Console.ReadLine());

            float[] inputs = new float[qtdInpts];
            float[] weights = new float[qtdInpts];
            float bias;

            Console.WriteLine("Escreva o valor dos Inputs como espaços entre eles: ");
            inputs = Converter(Console.ReadLine().Split());

            Console.WriteLine("Escreva o valor dos Pesos como espaços entre eles: ");
            weights = Converter(Console.ReadLine().Split());

            Console.WriteLine("Escreva aqui o valor da sua Bias: ");
            bias = float.Parse(Console.ReadLine());

            Console.WriteLine("Seu output é: " + CalculateOutput(inputs, weights, bias));

        }

        static float[] Converter(string[] s)
        {
            float[] temp = new float[s.Length];
            for (int i = 0; i < s.Length; i++)
            {
                temp[i] = float.Parse(s[i]);
            }

            return temp;
        }


        static float CalculateOutput(float[] inputs, float[] weights, float bias)
        {
            float sumReult = 0.0f;

            for (int i = 0; i < inputs.Length; i++)
            {
                sumReult += inputs[i] * weights[i];
            }

            sumReult = sumReult - bias;


            return 1 / (1 + MathF.Exp(-sumReult));

        }
    }
}
