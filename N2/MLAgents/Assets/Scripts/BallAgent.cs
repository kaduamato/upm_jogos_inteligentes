﻿using System.Collections;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;

public class BallAgent : Agent
{
    const float targetXMin = -4.5f;
    const float targetXMax = 4.5f;
    const float portalXMin = -4.0f;
    const float portalXMax = 4.0f;
    
    const float targetY = 0.5f;
    const float portalY = 1.0f;

    const float targetZMin = 8.5f;
    const float targetZMax = 17.5f;
    const float portalAZ = 5.25f;
    const float portalBZ = 7.75f;

    const float finalReward = 1.0f;

    Rigidbody rigidbody;

    [SerializeField] Transform targetCube;
    [SerializeField] Transform portalA;
    [SerializeField] Transform portalB;
    
    [SerializeField] float offset;
    [SerializeField] float speed;
    [SerializeField] float timeToTimeOut;
    [SerializeField] float passToPortalReward;
    [SerializeField] float distanceTargetLimit;

    bool passedPortalA;
    bool returnToStart;
    bool reward;

    Vector3 initialPos;

    Coroutine timeOut;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        initialPos = transform.localPosition;
    }

    public override void OnEpisodeBegin()
    {
        timeOut = StartCoroutine(TimeOut());

        if (transform.localPosition.y < 0.0f)
        {
            rigidbody.angularVelocity = Vector3.zero;
            rigidbody.velocity = Vector3.zero;
        }

        passedPortalA = false;
        returnToStart = false;
        reward = false;

        transform.localPosition = initialPos;
        targetCube.localPosition = new Vector3(Random.Range(targetXMin, targetXMax), targetY, Random.Range(targetZMin, targetZMax));
        portalA.localPosition = new Vector3(Random.Range(portalXMin, portalXMax), portalY, portalAZ);
        portalB.localPosition = new Vector3(Random.Range(portalXMin, portalXMax), portalY, portalBZ);
        
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation(targetCube.localPosition);
        sensor.AddObservation(portalA.localPosition);
        sensor.AddObservation(portalB.localPosition);
        sensor.AddObservation(transform.localPosition);

        sensor.AddObservation(rigidbody.velocity.x);
        sensor.AddObservation(rigidbody.velocity.z);
    }

    public override void OnActionReceived(float[] vectorAction)
    {
        Vector3 controlSignal = Vector3.zero;
        controlSignal.x = vectorAction[0];
        controlSignal.z = vectorAction[1];
        rigidbody.AddForce(controlSignal * speed);

        float distanceToTarget = Vector3.Distance(transform.localPosition, targetCube.transform.localPosition);

        if (distanceToTarget < distanceTargetLimit)
        {
            SetReward(finalReward);
            StopProcess();
            Debug.Log($"Ganhei {finalReward} de recompensa");
        }

        if (passedPortalA && !reward)
        {
            SetReward(passToPortalReward);
            reward = true;
            transform.localPosition = new Vector3(portalB.localPosition.x, transform.position.y, transform.localPosition.z + 2*offset);
            Debug.Log($"Ganhei {passToPortalReward} de recompensa");
        }

        if (transform.localPosition.y < 0.0f || returnToStart)
        {
            StopProcess();
        }
    }
    
    public override void Heuristic(float[] actionsOut)
    {
        actionsOut[0] = Input.GetAxis("Horizontal");
        actionsOut[1] = Input.GetAxis("Vertical");
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PortalA"))
        {
            passedPortalA = true;
        }

        if (other.gameObject.CompareTag("PortalB"))
        {
            returnToStart = true;
        }
        
    }

    void StopProcess()
    {
        StopCoroutine(timeOut);
        EndEpisode();
    }

    IEnumerator TimeOut()
    {
        Debug.Log("Corrotina");
        yield return new WaitForSeconds(timeToTimeOut);
        EndEpisode();
    }
}